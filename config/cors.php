<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    */
   // kok bisa ada file cors.php disini 
   // karena pake php artisan vendor:publish --provider="Barryvdh\Cors\ServiceProvider"

    'supportsCredentials' => false,
    'allowedOrigins' => ['*'], //['namadomain.com'],
    'allowedHeaders' => ['*'], //authorized
    'allowedMethods' => ['*'], //['POST']
    'exposedHeaders' => [],
    'maxAge' => 0,

];
